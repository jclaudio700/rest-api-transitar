package com.transitar.api.service;

import java.math.BigDecimal;
import java.util.List;

import com.transitar.api.exception.ServiceException;

public interface AvaliacaoService {

	public BigDecimal calcularMedia(List<String> notas) throws ServiceException;
	
	public String getAprovacao(BigDecimal media) throws ServiceException;
	
}
