package com.transitar.api.model;

import java.io.Serializable;

public class AlunoResponse implements Serializable {

	private static final long serialVersionUID = 1263531771802079331L;

	// Código de retorno do sistema.
	private String codigo;

	// Média do calculo das notas.
	private String media;
	
	// Statu de aprovação do aluno(Aprovado ou Reprovado). 
	private String statusAprovacao;
	
	// Mensagem de retorno do sistema.
	private String message;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getStatusAprovacao() {
		return statusAprovacao;
	}

	public void setStatusAprovacao(String statusAprovacao) {
		this.statusAprovacao = statusAprovacao;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
