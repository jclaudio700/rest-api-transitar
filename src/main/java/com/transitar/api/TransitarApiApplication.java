package com.transitar.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class TransitarApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransitarApiApplication.class, args);
		System.out.println("Executando aplicação mongodb.");
	}

}
