# API REST - Transitar

Projeto de API Rest, criado com Spring Boot para calcular a média das notas dos alunos.

# Documentação API

https://transitar-api.herokuapp.com/swagger-ui/index.html

# Desenvolvimento

Utilizado Spring Boot seguindo os padrões Rest e Swagger para Documentação.

# Setup Inicial

Para construir e executar o aplicativo, você precisa ter as seguintes dependências estejam corretamente instaladas:

    JDK 1.8.x
    Maven 3.x.x
    Mongodb

# Passo a passo para configuração

Clone o aplicativo

git clone : https://gitlab.com/jclaudio700/rest-api-transitar.git

# Construído com

Ferramentas que são utilizadas para criar o projeto.

    STS - Spring 4
    SpringBoot - Framework Java
    Swagger - Documentação da API
    Heroku

# Autor

Cláudio Gonçalves
