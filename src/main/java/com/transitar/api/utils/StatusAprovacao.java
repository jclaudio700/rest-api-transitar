package com.transitar.api.utils;

public enum StatusAprovacao {
	APROVADO, REPROVADO
}
