package com.transitar.api.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transitar.api.exception.ServiceException;
import com.transitar.api.helper.AlunoHelper;
import com.transitar.api.model.Aluno;
import com.transitar.api.model.AlunoRequest;
import com.transitar.api.model.AlunoResponse;
import com.transitar.api.repository.AlunoRepository;

@Service
public class AlunoServiceImpl implements AlunoService {

	@Autowired
	private AlunoRepository alunoRepository;
	
	@Autowired
	private AvaliacaoService avaliacaoService;

	@Override
	public AlunoResponse media(AlunoRequest request) {
		AlunoResponse response = new AlunoResponse();
		try {
			Aluno aluno = AlunoHelper.requestToModel( request );
			
			List<String> notas = Arrays.asList( 
					request.getNota1(), request.getNota2(), request.getNota3() );
			
			// Calculando média das notas.
			BigDecimal media = this.avaliacaoService.calcularMedia( notas );
			
			// Recuperando o staus de aprovação.
			String statusAprovacao = this.avaliacaoService.getAprovacao( media );
			
			aluno.setMedia( media.toString() );
			aluno.setStatusAprovacao( statusAprovacao );
			
			// Salvando os dados do aluno.
			Aluno newAluno = save( aluno );
			
			response.setCodigo( newAluno.getCodigo() );
			response.setMedia( media.toString() );
			response.setStatusAprovacao( statusAprovacao );
			response.setMessage("Operação realizada com sucesso.");

		} catch(Exception ex) {
			response.setMessage( ex.getMessage() );
		}
		return response;
	}
	
	/**
	 * Salva os dados do aluno na base de dados.
	 * @param aluno Aluno
	 * @return Aluno
	 * @throws ServiceException exception
	 */
	private Aluno save(Aluno aluno) throws ServiceException {
		try {
			return this.alunoRepository.save(aluno);
		} catch(Exception ex) {
			throw new ServiceException("Erro ao tentar salvar os dados aluno.");
		}
	}

}
