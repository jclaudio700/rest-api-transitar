package com.transitar.api.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.stereotype.Service;

import com.transitar.api.exception.ServiceException;
import com.transitar.api.utils.StatusAprovacao;

@Service
public class AvaliacaoServiceImpl implements AvaliacaoService {

	/**
	 * Realiza o cáculo da média das notas do aluno.
	 * @param notas List<String>
	 * @return BigDecimal
	 * @throws ServiceException exception
	 */
	@Override
	public BigDecimal calcularMedia(List<String> notas) throws ServiceException {
		BigDecimal media = new BigDecimal(0);
		try {
			BigDecimal total = new BigDecimal(0);
			for ( String nota : notas ) {
				total = total.add( new BigDecimal( nota ) );
			}

			media = new BigDecimal( total.toString() )
					.divide( new BigDecimal(notas.size() ), 2, RoundingMode.UP );
			
		} catch (Exception ex) {
			throw new ServiceException("Erro ao realizar o cálculo da média das notas do aluno.");
		}
		return media;
	}

	/**
	 * Retorna o status de aprovação do aluno. 
	 * Se média >= 7 aluno aprovado, se média < 7
	 * aluno reprovado.
	 * 
	 * @param media BigDecimal
	 * @return String
	 * @throws ServiceException exception
	 */
	@Override
	public String getAprovacao(BigDecimal media) throws ServiceException {
		String status = "";
		try {
			status = ( media.compareTo(new BigDecimal(7)) == 1 || 
					media.compareTo(new BigDecimal(7)) == 0 ) 
					? StatusAprovacao.APROVADO.toString() 
					: StatusAprovacao.REPROVADO.toString(); 
		} catch(Exception ex) {
			throw new ServiceException("Erro ao recuperar o status de aprovação do aluno.");
		}
		return status;
	}

}
