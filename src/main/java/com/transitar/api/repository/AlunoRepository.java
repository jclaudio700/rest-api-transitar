package com.transitar.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.transitar.api.model.Aluno;

@Repository
public interface AlunoRepository extends MongoRepository<Aluno, String> {
	
}
