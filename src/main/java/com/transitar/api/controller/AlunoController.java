package com.transitar.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transitar.api.model.AlunoRequest;
import com.transitar.api.model.AlunoResponse;
import com.transitar.api.service.AlunoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/alunos")
@Api(value = "Aluno")
public class AlunoController {

	@Autowired
	private AlunoService alunoService;
	
	@ApiOperation(value = "Realiza o cálculo da média das notas.")
	@PostMapping(value = "/media")
	public ResponseEntity<AlunoResponse> media(@RequestBody AlunoRequest request) {
		return ResponseEntity.ok( this.alunoService.media( request ) );
	}
	
}
