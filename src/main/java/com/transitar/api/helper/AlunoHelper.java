package com.transitar.api.helper;

import com.transitar.api.model.Aluno;
import com.transitar.api.model.AlunoRequest;

public class AlunoHelper {
	
	public static Aluno requestToModel(AlunoRequest alunoRequest) {
		Aluno aluno = new Aluno();
		aluno.setCodigo( 
			( alunoRequest.getCodigo() == null || alunoRequest.getCodigo().trim().isEmpty() ) 
			? null : alunoRequest.getCodigo()
		);
		aluno.setNome( alunoRequest.getNome() );
		aluno.setEmail( alunoRequest.getEmail() );
		aluno.setIdade( alunoRequest.getIdade() );
		aluno.setNascimento( alunoRequest.getNascimento() );
		aluno.setNota1( alunoRequest.getNota1() );
		aluno.setNota2( alunoRequest.getNota2() );
		aluno.setNota3( alunoRequest.getNota3() );
		return aluno;
	}
	
}
