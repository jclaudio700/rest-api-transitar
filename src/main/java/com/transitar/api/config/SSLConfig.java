package com.transitar.api.config;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SSLConfig {

	@PostConstruct
    private void configureSSL() {

        System.setProperty("https.protocols", "TLSv1.2");
    }
	
}
