package com.transitar.api.service;

import com.transitar.api.model.AlunoRequest;
import com.transitar.api.model.AlunoResponse;

public interface AlunoService {
	
	public AlunoResponse media(AlunoRequest request);
	
}
